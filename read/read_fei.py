import os

import cv2
from image import Image
from pathlib import Path

_dir_path = 'images/fei/spatially_normalized/'
_average_landmarks_path = 'cached_data/average_landmarks.dat'
train_shape = (300, 250)


# train_shape = (193, 162)


def read_metadata():
    lines = []
    with open(_dir_path + 'metadata.txt') as file:
        for line in file:
            file_name, gender = line.strip().split('\t')
            lines.append({'file_name': file_name, 'gender': ord(gender)})
    return lines


def read_images(metadata):
    images = []
    for m in metadata:
        image_path = _dir_path + m['file_name']
        image = cv2.imread(image_path, 0)
        images.append(Image(image, m))
    return images


def read_image(metadata):
    image_path = _dir_path + metadata['file_name']
    image = cv2.imread(image_path, 0)
    return Image(image, metadata)


def make_metadata():
    metadata_file = Path(_dir_path + 'metadata.txt')
    if not metadata_file.is_file():
        with open(_dir_path + 'metadata.txt', 'w') as f:
            for file in os.listdir(_dir_path):
                if file.endswith('.jpg'):
                    print(file)
                    image = cv2.imread(_dir_path + file)
                    cv2.imshow(file, image)
                    k = cv2.waitKey(0) & 0xFF
                    if k == ord('m'):
                        print('\t' + 'm')
                        f.write(file + '\t' + 'm')
                    elif k == ord('f'):
                        print('\t' + 'f')
                        f.write(file + '\t' + 'f')
                    cv2.destroyAllWindows()


def read_negative_images():
    negative_dir = 'images/fei/negative/'
    images = []
    skip = 0
    for file in os.listdir(negative_dir):
        if file.endswith('.jpg') and skip % 1 == 0:
            image = cv2.imread(negative_dir + file)
            for i in range(0, image.shape[1] - 161, 162):
                for j in range(0, image.shape[0] - 192, 193):
                    cut = image[j:j + 193, i:i + 162]
                    images.append(Image(cut, {'negative': True}))
            skip += 1
    return images


def is_cached():
    average_file = Path(_average_landmarks_path)
    return average_file.is_file()


def read_average_landmarks():
    average_file = Path(_average_landmarks_path)
    average_landmarks = []
    with average_file.open() as f:
        for line in f:
            x, y = line.split()
            average_landmarks.append((int(x), int(y)))
    return average_landmarks


def save_average_landmarks(landmarks):
    with open(_average_landmarks_path, 'w') as f:
        for x, y in landmarks:
            f.write(str(x) + ' ' + str(y) + '\n')
