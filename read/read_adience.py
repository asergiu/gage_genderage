import cv2

from image import Image

_blacklist = []


def read_metadata():
    path = 'images/adience/'
    lines = []
    for i in range(0, 5):
        file_name = path + 'fold_frontal_' + str(i) + '_data.txt'
        file = open(file_name)
        header = file.__next__().split('\t')
        for line in file:
            line = line.split('\t')
            image = {}
            for l, label in enumerate(header):
                if line[l] == '':
                    continue
                if label == 'gender':
                    image[label] = ord(line[l])
                else:
                    image[label] = line[l]
            if 'gender' in image.keys() and (image['gender'] == ord('f') or image['gender'] == ord('m')):
                if not blacklisted(image):
                    lines.append(image)
    print("Total number of images", len(lines))
    return lines


def read_image(fold):
    """

    :rtype: Image
    """
    dir_path = 'images/adience/aligned/'
    image_path = dir_path + fold['user_id'] + '/landmark_aligned_face.' + fold['face_id'] + '.' + fold['original_image']
    image = cv2.imread(image_path, 0)
    return Image(image, fold)


def read_images(folds):
    """

    :rtype: list of Image
    """
    dir_path = 'images/adience/aligned/'
    images = []
    for im in folds:
        image_path = dir_path + im['user_id'] + '/landmark_aligned_face.' + im['face_id'] + '.' + im['original_image']
        image = cv2.imread(image_path, 0)
        images.append(Image(image, im))
    return images


def blacklist(image):
    with open('images/adience/blacklist.txt', 'a') as file:
        name = image.metadata['user_id'] + '\t' + image.metadata['face_id'] + '\t' + image.metadata['original_image']
        file.write(name + '\n')


def _read_blacklist():
    with open('images/adience/blacklist.txt') as file:
        for line in file:
            user_id, face_id, original_image = line.strip().split('\t')
            _blacklist.append({'user_id': user_id, 'face_id': face_id, 'original_image': original_image})


def blacklisted(image):
    for line in _blacklist:
        if line['user_id'] == image['user_id'] \
                and line['face_id'] == image['face_id'] \
                and line['original_image'] == image['original_image']:
            return True
    return False

_read_blacklist()
