import cv2
import matplotlib.pyplot as plot
import time
import copy

from align import affine_transform, equalize, crop, perspective_transform, Align
from facial_landmarks import get_average_landmarks_for_training_set, get_facial_landmarks, find_faces
from classify import predict_gender, prepare_classifier


def run_tests(test_dir='images/test/', test_images=None, align=True):
    if align:
        results = {Align.AFFINE: [], Align.PERSPECTIVE: []}
    else:
        results = {Align.NO_ALIGN: []}
    average_time = []

    if test_images is None:
        test_images = [{'file_name': line[2:-1], 'gender': line[0]} for line in open(test_dir + 'metadata.txt')]

    face_no = 1
    for face in test_images:
        print(face_no, end=' ', flush=True)
        face_no += 1
        # print(face['file_name'], face['gender'])
        img = cv2.imread(test_dir + face['file_name'])
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        if align:
            result, prediction_time = run(img)

            results[Align.AFFINE].append(result[Align.AFFINE] == face['gender'])
            results[Align.PERSPECTIVE].append(result[Align.PERSPECTIVE] == face['gender'])
        else:
            start_prediction = time.time()
            results[Align.NO_ALIGN].append(predict_gender(img) == face['gender'])
            end_prediction = time.time()
            prediction_time = end_prediction - start_prediction

        average_time.append(prediction_time)

    print('\nResults:')
    for key, value in results.items():
        s = sum(1 if r is True else 0 for r in value)
        print('\t' + key.name + ':', s / len(value) * 100)
    print('Average prediction time:', sum(average_time) / len(average_time))

    result_key = Align.PERSPECTIVE if align else Align.NO_ALIGN
    return sum(1 if r is True else 0 for r in results[result_key]) / len(results[result_key]) * 100


def run(img, show_gui=False):
    result = {Align.AFFINE: None, Align.PERSPECTIVE: None}

    average = get_average_landmarks_for_training_set()
    landmarks_img = get_facial_landmarks(img)

    affine_face = affine_transform(img, landmarks_img, average)
    affine_face = equalize(affine_face)
    affine_crop = crop(affine_face, get_facial_landmarks(affine_face))

    landmarks_affine = get_facial_landmarks(affine_face)
    perspective_face = perspective_transform(affine_face, landmarks_affine, average)
    perspective_crop = crop(perspective_face, get_facial_landmarks(perspective_face))

    result[Align.AFFINE] = predict_gender(affine_crop)
    start_prediction = time.time()
    result[Align.PERSPECTIVE] = predict_gender(perspective_crop)
    end_prediction = time.time()
    prediction_time = end_prediction - start_prediction

    if show_gui:
        print('\taffine prediction:', result[Align.AFFINE])
        print('\tperspective prediction:', result[Align.PERSPECTIVE])

        reference = cv2.imread('images/fei/spatially_normalized/1a.jpg', 0)
        reference = equalize(reference)
        reference = crop(reference, get_facial_landmarks(reference))

        plot.subplot(141), plot.imshow(img, cmap='gray'), plot.title('Input')
        plot.subplot(142), plot.imshow(reference, cmap='gray'), plot.title('Ref')
        plot.subplot(143), plot.imshow(affine_crop, cmap='gray'), plot.title(Align.AFFINE)
        plot.subplot(144), plot.imshow(perspective_crop, cmap='gray'), plot.title(Align.PERSPECTIVE)
        mng = plot.get_current_fig_manager()
        mng.resize(width=1500, height=800)
        plot.show()

    return result, prediction_time


def put_labels(original):
    gray = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)

    faces = find_faces(gray)
    for landmarks in faces:
        original_landmarks = landmarks
        text_point = landmarks[16]
        predictions = []

        try:
            result, prediction_time = run(affine_transform(gray, landmarks, get_average_landmarks_for_training_set()))
            predictions.append(result[Align.PERSPECTIVE])
        except AssertionError:
            predictions.append('N')

        predictions = ''.join(predictions).upper()
        cv2.putText(original, predictions, (text_point[0], text_point[1] - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 255), 2)

        for x, y in original_landmarks:
            cv2.circle(original, (int(x), int(y)), 2, (0, 0, 255), -1)

    scale_factor = 950 / original.shape[0]
    if original.shape[0] > 950:
        original = cv2.resize(original, (0, 0), fx=scale_factor, fy=scale_factor)

    return original


def run_for_urls():
    import urllib.request

    with open('images/urls') as file:
        for line in file:
            if line.startswith('='):
                break

            urllib.request.urlretrieve(line, 'images/d_img.jpg')
            img = cv2.imread('images/d_img.jpg')

            labeled = put_labels(img)

            cv2.imshow('Output', labeled)
            cv2.waitKey(0)
            cv2.destroyAllWindows()


def k_fold_cross_validation():
    from read.read_fei import read_metadata

    k = 400 // 50
    all = read_metadata()
    female = [img for img in all if img['gender'] == ord('f')]
    male = [img for img in all if img['gender'] == ord('m')]
    assert len(male) == len(female) == 200

    all = []
    for i in range(len(male)):
        all.append(male[i])
        all.append(female[i])

    results = []
    for i in range(k):
        all_copy = copy.deepcopy(all)
        test = all_copy[i*50:(i+1)*50]
        training = all_copy[:i*50] + all[(i+1)*50:]
        prepare_classifier(training, False)
        print('\t', i, 'out of', k, ' | ', end='', flush=True)

        for j in range(len(test)):
            test[j]['gender'] = chr(test[j]['gender'])

        r = run_tests('images/fei/spatially_normalized/', test, False)
        results.append(r)

    print('K result:', sum(results) / len(results))

# prepare_classifier()

# run_for_urls()
# train_fisherface()
# run()
# run_tests()
# k_fold_cross_validation()
