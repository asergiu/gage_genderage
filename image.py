import numpy as np


class Image:
    def __init__(self, image, metadata):
        self.image = image
        self.metadata = metadata


def extract_labels(images, label):
    """

    :param label: string
    :type images: Image[]
    """
    _cv2_images = []
    _labels = []
    for image in images:
        _cv2_images.append(image.image)
        _labels.append(image.metadata[label])
    return _cv2_images, np.array(_labels)
