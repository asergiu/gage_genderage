from enum import Enum

import cv2
import numpy

from facial_landmarks import get_average_landmarks_for_training_set

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))


class Align(Enum):
    AFFINE = 1
    PERSPECTIVE = 2
    NO_ALIGN = 3


def affine_transform(img, landmarks, average):
    padding = 100
    padded_average = [(x+padding, y+padding) for x, y in average]
    source_points = numpy.float32([landmarks[39], landmarks[42], landmarks[57]])
    destination_points = numpy.float32([padded_average[39], padded_average[42], padded_average[57]])

    m = cv2.getAffineTransform(source_points, destination_points)
    affine_face = cv2.warpAffine(img, m, (250 + 2*padding, 300 + 2*padding))

    return affine_face


def perspective_transform(img, landmarks, average):
    padding = 100
    padded_average = [(x+padding, y+padding) for x, y in average]
    source_points = numpy.float32([landmarks[39], landmarks[7], landmarks[9], landmarks[42]])
    destination_points = numpy.float32([padded_average[39], padded_average[7], padded_average[9], padded_average[42]])

    m = cv2.getPerspectiveTransform(source_points, destination_points)
    perspective_face = cv2.warpPerspective(img, m, (250 + 2*padding, 300 + 2*padding))

    return perspective_face


def crop(img, landmarks):
    average = get_average_landmarks_for_training_set()
    jaw_width = average[16][0] - average[0][0]
    scale_factor = jaw_width / (landmarks[16][0] - landmarks[0][0])
    img = cv2.resize(img, (0, 0), fx=scale_factor, fy=scale_factor)

    landmarks = [(int(x * scale_factor), int(y * scale_factor)) for x, y in landmarks]

    y = landmarks[0][1] - int(0.4 * (average[8][1] - average[27][1]))
    x = landmarks[0][0]
    w = jaw_width
    h = average[8][1] - (average[0][1] - int(0.4 * (average[8][1] - average[27][1])))

    cr = img[y:y + h, x:x + w].copy()

    return cr


def equalize(img):
    return clahe.apply(img)
