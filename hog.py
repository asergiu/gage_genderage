import cv2
import numpy

from read.read_fei import read_images, read_metadata, read_negative_images

winSize = (160, 192)
blockSize = (16, 16)
blockStride = (8, 8)
cellSize = (8, 8)
nbins = 9
derivAperture = 1
winSigma = 4.
histogramNormType = 0
L2HysThreshold = 2.0000000000000001e-01
gammaCorrection = True


def detect_video(hog):
    cap = cv2.VideoCapture(0)

    while True:
        ret, frame = cap.read()
        img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        locations, weights = hog.detectMultiScale(img, winStride=(8, 8), scale=1.5)
        for x, y, w, h in locations:
            cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 1)

        cv2.imshow('frame', img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


def detect_image(hog):
    img = cv2.imread('images/fei/will.jpeg')
    locations, weights = hog.detectMultiScale(img, winStride=(8, 8), scale=1.01)
    print(locations)

    # for x, y, w, h in locations:
    #     cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 1)
    #
    # cv2.imshow('img', img)
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    return locations


def hog_detection():
    hog = cv2.HOGDescriptor(winSize, blockSize, blockStride, cellSize, nbins, derivAperture, winSigma,
                            histogramNormType, L2HysThreshold, gammaCorrection)

    fold = read_metadata()
    positive = read_images(fold)
    negative = read_negative_images()
    images = positive + negative

    descriptors = []
    for img in images:
        descriptor = hog.compute(img.image)
        descriptors.append(descriptor)

    labels = numpy.array([1] * len(positive) + [-1] * len(negative))

    train_data = numpy.empty((0, len(descriptors[0])), dtype=numpy.float32)
    for descriptor in descriptors:
        train_data = numpy.vstack((train_data, descriptor.transpose()))

    svm = cv2.ml.SVM_create()
    svm.setCoef0(0)
    svm.setDegree(3)
    svm.setTermCriteria((cv2.TERM_CRITERIA_MAX_ITER + cv2.TERM_CRITERIA_EPS, 1000, 1.e-3))
    svm.setGamma(0)
    svm.setKernel(cv2.ml.SVM_LINEAR)
    svm.setNu(0.5)
    svm.setP(0.1)
    svm.setC(0.01)
    svm.setType(cv2.ml.SVM_EPS_SVR)
    svm.train(train_data, cv2.ml.ROW_SAMPLE, labels)

    rho, alpha, svidx = svm.getDecisionFunction(0)
    hog.setSVMDetector(numpy.append(svm.getSupportVectors(), -rho))

    return detect_image(hog)
    # return detect_video(hog)


hog_detection()
