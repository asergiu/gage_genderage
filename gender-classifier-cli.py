#!/home/alex/.virtualenvs/cv/bin/python3

import re
import sys

import cv2

import classify
import controller

options = {}


def show_help():
    print('gender-classifier-cli [OPTION]')
    print('The following options are available:')
    print('\t-l\t\ttrain classifier')
    print('\t-k\t\trun k fold cross validation')
    print('\t-t\t\trun tests')
    print('\t-u\t\trun on images given as urls in images/urls')
    print('\t-r={path}\trun for single image')
    print('\t-h\t\tdisplay this help screen and exit')


def bad_argument():
    print('Bad argument: ', option)
    print('Use option -h for help')
    exit(1)

for option in sys.argv[1:]:
    attribute = re.search('-.+=', option)
    if attribute:
        attribute = option.split('=')[0][1:]
        options[attribute] = option.split('=')[1]
    elif re.search('-.+', option):
        options[option[1:]] = True
    else:
        bad_argument()

if 'h' in options.keys() or len(options.keys()) == 0:
    show_help()
if 'l' in options.keys():
    classify.prepare_classifier()
if 'u' in options.keys():
    classify.prepare_classifier(use_cached_training_data=True, force_average=False)
    controller.run_for_urls()
elif 'k' in options.keys() and options['k'] is True:
    controller.k_fold_cross_validation()
elif 't' in options.keys():
    classify.prepare_classifier()
    controller.run_tests()
elif 'r' in options.keys():
    classify.prepare_classifier(use_cached_training_data=True, force_average=False)
    controller.run(cv2.cvtColor(cv2.imread(options['r']), cv2.COLOR_BGR2GRAY), True)
# else:
#     bad_argument()
