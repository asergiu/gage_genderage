import time

import cv2

import facial_landmarks
from image import extract_labels
from read.read_fei import read_images, read_image, read_metadata

prediction_classifier = None
training_data_path = 'cached_data/training.dat'


def prepare_classifier(training_fold=None, crop=True, equalize=True, use_cached_training_data=False, force_average=True):
    global prediction_classifier
    facial_landmarks.get_average_landmarks_for_training_set(force_average)
    prediction_classifier = cv2.face.createLBPHFaceRecognizer()
    if not use_cached_training_data:
        train_classifier(training_fold, crop, equalize)
    prediction_classifier.load(training_data_path)


def train_classifier(fold=None, crop=True, equalize=True):
    import align

    if fold is None:
        fold = read_metadata()

    print('Training...')
    images = read_images(fold)
    cv2_images, labels = extract_labels(images, 'gender')

    if crop:
        average = facial_landmarks.get_average_landmarks_for_training_set()
        y = average[0][1] - int(0.4 * (average[8][1] - average[27][1]))
        x = average[0][0]
        w = average[16][0] - average[0][0]
        h = average[8][1] - y

        cv2_images = [img[y:y + h, x:x + w].copy() for img in cv2_images]

    if equalize:
        cv2_images = [align.equalize(img) for img in cv2_images]

    start_training_time = time.time()
    prediction_classifier.train(cv2_images, labels)
    end_training_time = time.time()

    print('Training time:', end_training_time - start_training_time)

    prediction_classifier.save(training_data_path)


def predict_gender(img):
    label = prediction_classifier.predict(img)

    return chr(label[0])


def fisherface():
    fold = read_metadata()
    # random.shuffle(fold)
    number_of_images = len(fold)
    print('Number of images: ', number_of_images)
    training_size = int(number_of_images * 0.9)

    recognizer = cv2.face.createFisherFaceRecognizer()

    print('Training...')
    # for i in range(0, training_size, 400):
    #     line = fold[i:i + min(400, training_size - i)]
    images = read_images(fold)
    cv2_images, labels = extract_labels(images, 'gender')
    recognizer.train(cv2_images[:training_size], labels[:training_size])

    print('Predicting...')
    correct_predictions = 0
    average_confidence = 0
    for fold in fold[training_size:]:
        image = read_image(fold)
        predicted_label = recognizer.predict(image.image)
        average_confidence += predicted_label[1]
        if predicted_label[0] == image.metadata['gender']:
            correct_predictions += 1
            # else:
            #     print(image.metadata['file_name'] + ' predicted: ' + chr(predicted_label[0]),
            #       'expected: ' + chr(image.metadata['gender']))
            #     cv2.imshow("Wrong guess", image.image)
            #     cv2.waitKey(0) & 0xFF
    average_confidence /= number_of_images - training_size

    print("Result: " + str(correct_predictions) +
          " correct predictions out of " + str(number_of_images - training_size))
    print(correct_predictions * 100 / (number_of_images - training_size))
    print("Average confidence: ", average_confidence)
    return correct_predictions * 100 / (number_of_images - training_size)


def show_all_images():
    folds = read_metadata()
    for fold in folds:
        image = read_image(fold)
        cv2.imshow("img", image.image)
        k = cv2.waitKey(0) & 0xFF
        if k == ord('b'):
            print("blacklisted", fold)

# show_all_images()
