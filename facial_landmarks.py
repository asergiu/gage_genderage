import cv2
import dlib
from imutils import face_utils

predictor_path = 'cached_data/shape_predictor_68_face_landmarks.dat'

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(predictor_path)


def get_average_landmark_location(landmarks):
    average = [(0, 0)] * len(landmarks[0])
    for l in landmarks:
        for i in range(len(l)):
            average[i] = (average[i][0] + l[i][0], average[i][1] + l[i][1])

    for i in range(len(average)):
        average[i] = (average[i][0] // len(landmarks), average[i][1] // len(landmarks))

    return average


def get_facial_landmarks(image):
    rects = detector(image, 1)

    if len(rects) != 1:
        print('More than one face: ', len(rects), '. Expecting only one')
    assert len(rects) == 1

    rect = rects[0]
    shape = predictor(image, rect)
    shape = face_utils.shape_to_np(shape)

    return shape


def find_faces(image):
    rects = detector(image, 1)
    faces = [face_utils.shape_to_np(predictor(image, rect)) for rect in rects]

    return faces


def get_face_rectangle(image):
    rects = detector(image, 1)

    assert len(rects) == 1

    rect = rects[0]
    x, y, w, h = face_utils.rect_to_bb(rect)
    crop = image[y:y + h, x:x + w]

    return crop


def get_average_landmarks_for_training_set(force_update=False):
    from read.read_fei import read_metadata, read_images, is_cached, read_average_landmarks, save_average_landmarks

    if is_cached() and not force_update:
        average = read_average_landmarks()
    else:
        print('Finding average landmarks...')
        fold = read_metadata()
        images = read_images(fold)
        landmarks_list = []
        for image in images:
            landmarks = get_facial_landmarks(image.image)
            landmarks_list.append(landmarks)

        average = get_average_landmark_location(landmarks_list)
        save_average_landmarks(average)

    return average


def facial_landmarks():
    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(predictor_path)

    image = cv2.imread('images/test/will-2.png')
    image = cv2.imread('images/test/george.jpg')
    image = cv2.imread('images/test/obama.jpg')
    image = cv2.imread('images/test/ed.jpg')
    image = cv2.imread('images/test/spatially_normalized/1a.jpg')
    image = cv2.imread('images/test/audrey.jpg')
    image = cv2.imread('images/test/t1.jpg')
    image = cv2.imread('images/test/will.jpeg')
    image = cv2.imread('images/test/me.jpeg')
    image = cv2.imread('images/test/jessica.jpg')
    image = cv2.imread('images/test/chad.png')
    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rects = detector(grey, 1)

    for i, rect in enumerate(rects):
        shape = predictor(grey, rect)
        shape = face_utils.shape_to_np(shape)

        x, y, w, h = face_utils.rect_to_bb(rect)
        crop = grey[y:y + h, x:x + w]
        # label = predict_gender(crop)
        # print(label)

        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

        for px, py in shape:
            cv2.circle(image, (px, py), 1, (0, 0, 255), -1)

    cv2.imshow("Output", image)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
